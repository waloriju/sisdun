json.array!(@debts) do |debt|
  json.extract! debt, :order_id, :data_vencimento, :data_pagamento, :valor_toral, :valor_pago, :valor_restante
  json.url debt_url(debt, format: :json)
end