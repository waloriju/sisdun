class Order < ActiveRecord::Base
  has_one :debt, dependent: :destroy
  has_many :line_items, dependent: :destroy
  belongs_to :customer
  #belongs_to :company
  
  accepts_nested_attributes_for :debt

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  def total(debt, order)
    debt.total =  order.price
    debt.remaining_amount = order.price
  end
end