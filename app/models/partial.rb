class Partial < ActiveRecord::Base
  belongs_to :debt
  validate :debt_id, presence: true
  validates :value, presence: true,
                    numericality: {greater_than_or_equal_to: 0.01}
end
