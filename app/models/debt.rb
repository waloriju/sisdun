class Debt < ActiveRecord::Base
  has_many :partials, dependent: :destroy
  belongs_to :order
  validate :expiration_date_future, allow_blank: true

  def expiration_date_future
    if expiration_date.present? && expiration_date < Date.today
      errors.add(:expiration_date, "Data deve ser futura")
    end
  end
end
