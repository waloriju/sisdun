class PartialsController < ApplicationController
  before_action :signed_in_user

  def index
  end

  def new
    @partial = Partial.new
  end

  def create
    @partial = Partial.new(partial_params)

    respond_to do |format|
      if @partial.save
        
        format.html { redirect_to debts_url }
        flash[:success] = "Recebimento efetuado com Sucesso!"
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def partial_params
      params.require(:partial).permit(:debt_id, :value, debt_attributes:[:date_payment, :total, :amount_paid, :remaining_amount], order_attributes:[:status])
    end
end