class CreateDebts < ActiveRecord::Migration
  def change
    create_table :debts do |t|
      t.references :order, index: true
      t.date :expiration_date #data de vencimento
      t.date :date_payment #data de pagamento
      t.decimal :total, precision: 8, scale: 2 #valor total
      t.decimal :amount_paid, precision: 8, scale: 2 #valor pago
      t.decimal :remaining_amount, precision: 8, scale: 2 #valor restante

      t.timestamps
    end
  end
end
