class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :customer, index: true
      t.decimal    :price, precision: 8, scale: 2
      t.string     :status, default: "Aberta"

      t.timestamps
    end
  end
end
