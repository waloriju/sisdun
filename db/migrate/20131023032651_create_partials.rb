class CreatePartials < ActiveRecord::Migration
  def change
    create_table :partials do |t|
      t.decimal :value, precision: 8, scale: 2
      t.references :debt

      t.timestamps
    end
    add_index :partials, [:debt_id, :created_at]
  end
end
